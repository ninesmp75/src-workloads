# EMUCAT ASKAP Pipeline

This is a cutback version of the EMUCat pipeline downloads an ASKAP observation, generates a mosaic and then performs a radio source extraction. 

## Requirements
 * SLURM HPC Cluster
 * Nextflow >= 21.04 module
 * OpenMPI >= 4.1 module
 * Singularity module

## Running

```
module load nextflow
module load singularity

nextflow run main.nf  -profile generic --SCRATCH_ROOT=/scratch/run/
```

* profile: nextflow.conf profile (default: generic)
* scratch_root: the base directory for all the data (default: /mnt/shared/test/)