#!/bin/bash
# check to pull latest version, if needed
docker pull registry.gitlab.com/ska-telescope/src/src-workloads/image-coadding-swarp:latest
# run container pipeline
docker run -it --rm --name image-coadding-swarp -t -v "$(pwd)"/scripts:/scripts/ registry.gitlab.com/ska-telescope/src/src-workloads/image-coadding-swarp:latest
