#!/bin/bash
# check to pull latest version, if needed
docker pull registry.gitlab.com/ska-telescope/src/src-workloads/mosaicking-swarp:latest
# run container pipeline
docker run -it --rm --name mosaicking-swarp -v "$(pwd)"/scripts:/scripts/ registry.gitlab.com/ska-telescope/src/src-workloads/mosaicking-swarp:latest
