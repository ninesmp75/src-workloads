#!/bin/bash

echo "This script will make the LOFAR mosaic."

# fix permissions
chmod 755 * -R

# Make LOFAR mosaic
cd lofar
# get data
echo "downloading LOFAR data..."
./get-lofar-data-parallel.sh lofar-files.csv
# do mosaicking
echo "making LOFAR mosaic..."
swarp `ls P*.fits`  # use default swarp parameters for lofar images
