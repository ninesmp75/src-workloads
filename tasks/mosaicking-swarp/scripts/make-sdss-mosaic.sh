#!/bin/bash

echo "This script will make the SDSS mosaic."

# fix permissions
chmod 755 * -R

# Make SDSS mosaic
cd sdss
# get data
echo "downloading SDSS data..."
./get-sdss-data-parallel.sh J011700.00+280000.0.small.sh
# do mosaicking
echo "making SDSS mosaic..."
./J011700.00+280000.0.small.sh # swarp parameters for SDSS images built into this run script
