#!/bin/bash
# check to pull latest version, if needed
docker pull registry.gitlab.com/ska-telescope/src/src-workloads/source-finding-pybdsf:latest
# run container pipeline
docker run -it --rm --name source-finding-pybdsf -v "$(pwd)"/scripts:/scripts/ registry.gitlab.com/ska-telescope/src/src-workloads/source-finding-pybdsf:latest
