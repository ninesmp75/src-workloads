#!/bin/bash
# check to pull latest version, if needed
docker pull registry.gitlab.com/ska-telescope/src/src-workloads/cnn-classifier-mirabest:latest
# run container pipeline
docker run -it --rm --name cnn-classifier-mirabest -t -v "$(pwd)"/scripts:/scripts/ registry.gitlab.com/ska-telescope/src/src-workloads/cnn-classifier-mirabest:latest
