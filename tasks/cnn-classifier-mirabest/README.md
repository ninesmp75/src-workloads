# Radio galaxy image classification task, using a CNN and the MiraBest dataset. 

This task will train and test a CNN classifier on the MiraBest dataset of radio galaxies from the FIRST & NVSS radio surveys. Images are 150 pixels across, and are classified as either an FRI or FRII radio galaxy. 

We use code from Hongming Tang: https://github.com/HongmingTang060313/FR-DEEP/blob/master/FRDEEP_tutorial_FIRST.ipynb

We use code from Fiona Porter: https://github.com/fmporter/MiraBest_builder/blob/main/MiraBest_F.py


## How to run the task

Clone this repository and add execute permissions to files in the repository

    chmod 755 -R *

The run.sh script will pull the latest container from the registry (no building is required), and execute the task.

    ./run.sh

This task will download the MiraBest dataset of radio galaxy images, and train and test a CNN classifier, outputting its accuracy. 

## How to use a Jupyter notebook

You can use a Jupyter notebook to run this task interactively.

To use a Jupyter notebook environment, first build the Jupyter container locally since this is not in the container repository

    docker build . -f ./Dockerfile-jhub --tag cnn-classifier-mirabest-jhub:latest

then run the container and a link will appear which you can copy into your browser to access

    docker run -p 8888:8888 -v "$(pwd)"/scripts:/scripts --user root -e GRANT_SUDO=yes cnn-classifier-mirabest-jhub

You can then run through the .ipynb which does the same as the run.sh, but with additional plots and interactive elements to show what is happening.




--- notes ---

ssh -i ~/.ssh/id_rsa -L 8888:localhost:8888 dmz
ssh -i ~/.ssh/id_rsa -L 8888:localhost:8888 192.168.50.141

docker run -p 8888:8888 -v "$(pwd)"/scripts:/scripts --user root -e GRANT_SUDO=yes cnn-classifier

MiraBest_F uses 9 classes, but functionally you only train 3 classes: FRI, FRII, Hybrid.

We do from MiraBest_F import MBFRFull as MiraBest beacuse the MBFRFull function only selects binary classes in the data (FRI/FRII)

GPU use?

use_cuda = torch.cuda.is_avialable()
device = torch.device("cude" if use_cuda else "cpu")
kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {'num_workers': 3, 'pin_memory': False}

model = LeNet(in_chan=1, out_chan=num_classes, imsize=imsize, kernel_size=5, dropout=dropout).to(device=device)

set data devices 

for batch, (x_train, y_train) in enumerate(trainloader):
            x_train = x_train.to(device=device)
            y_train = y_train.to(device=device)









