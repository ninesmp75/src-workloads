import unittest
from unittest import mock
import stars
import io
import numpy as np
import logging
import time

class TestStars(unittest.TestCase):

    def setUp(self):
        # Common setup tasks
        logging.getLogger().setLevel(logging.DEBUG)

     # Test for read_yaml(file) with actual suite.yaml
    def test_read_yaml_with_suite_file(self):
        result = stars.read_yaml('suite.yaml')
        self.assertIsNotNone(result)  # Assuming suite.yaml is not empty

    # Test for read_yaml(path) with invalid content
    @mock.patch('builtins.open', new_callable=mock.mock_open, read_data=': invalid YAML')
    @mock.patch('sys.exit')
    def test_read_yaml_invalid(self, mock_exit, mock_open):
        # Temporarily set logging level to CRITICAL to suppress ERROR messages
        logging.getLogger().setLevel(logging.CRITICAL)
        stars.read_yaml('dummy.yaml')  # Path is irrelevant here, content is mocked
        mock_exit.assert_called_once()  # Check that sys.exit was called due to invalid YAML

    @mock.patch('stars.subprocess.run')
    @mock.patch('stars.os.path.isfile')
    def test_run_benchmark_valid(self, mock_isfile, mock_run):
        mock_isfile.return_value = True  # Simulate script exists
        mock_run.return_value = None  # Simulate successful script run
        result = stars.run_benchmark('path/to/script', 1, 0)
        self.assertIsInstance(result, list)

    @mock.patch('stars.subprocess.run')
    @mock.patch('stars.os.path.isfile')
    @mock.patch('stars.logging')
    def test_run_benchmark_failure(self, mock_logging, mock_isfile, mock_run):
        mock_isfile.return_value = True  # Simulate script exists
        mock_run.side_effect = stars.subprocess.CalledProcessError(1, 'cmd')
        with self.assertRaises(SystemExit):
            stars.run_benchmark('path/to/failing/script', 1, 0)
        mock_logging.error.assert_called_once()


    @mock.patch('stars.subprocess.run')
    @mock.patch('stars.os.path.isfile')
    def test_mock_runscripts(self, mock_isfile, mock_run):
        mock_isfile.return_value = True  # Simulate script exists
        mock_run.return_value = None  # Simulate successful script run
        stars.run_benchmark('path/to/script', 1, 0)
        mock_run.assert_called_with('path/to/script', shell=True, check=True)


    @mock.patch('stars.argparse.ArgumentParser.parse_args')
    @mock.patch('stars.subprocess.run')
    def test_main_valid(self, mock_run, mock_args):
        mock_run.return_value = None  # Simulate successful script run
        # Set the return_value of the mock_args to simulate a command line argument,
	    # passing "suite.yaml" as a positional argument as the yaml_file parameter of argparse.
        def mock_run_side_effect(*args, **kwargs):
            time.sleep(0.11)  # Simulates a brief, non-zero runtime
            return None
        mock_run.side_effect = mock_run_side_effect
        mock_args.return_value = mock.Mock(runtimes_only=False, yaml_file='suite.yaml')
        with mock.patch('sys.exit') as mock_exit:
            stars.main()
            mock_exit.assert_not_called()

    @mock.patch('stars.argparse.ArgumentParser.parse_args')
    @mock.patch('stars.subprocess.run')
    def test_main_runtimes_only(self, mock_run, mock_args):
        # Simulate subprocess.run taking 1 second without actual execution
        def mock_run_side_effect(*args, **kwargs):
            time.sleep(0.1)
            return None

        mock_run.side_effect = mock_run_side_effect

        # Simulate successful script run
        mock_run.return_value = None

        # Set the return_value of the mock_args to simulate --runtimes-only command line argument
        mock_args.return_value = mock.Mock(runtimes_only=True, yaml_file='suite.yaml')

        with mock.patch('sys.exit') as mock_exit:
            stars.main()
            mock_exit.assert_not_called()

        # Assert that subprocess.run and time.sleep were called
        self.assertTrue(mock_run.called)
