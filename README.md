- [SRC Scientific Workflows](#src-scientific-use-cases)
- [Directory structure](#directory-structure)
- [Adding a new applications or workflow](#adding-a-new-applications-or-workflow)

# SRC Scientific Workloads

## Introduction 

The goal of this repository is to collect runnable scientific **workloads** that will be expected to run at SKA Regional Centers (SRC). We classify workloads into **tasks** and **workflows**. A task is a small piece of code that achieves a step towards a science result, this could be source-finding, mosaicking, image cutting, etc. A workflow is a collection of these tasks that achieve a more detailed scientific result, and we are investigating using a workflow language to achieve this.

Each of the tasks and workflows should contain all the necessary information to be reproduced. This means any software required should be in a container with the Docker file described in the repository. The steps to build and run the Docker file should be stated, along with any commands the user should type inside the container. For example, bash or python commands should be described. Equally, required input data should be appropriately referenced along with download instructions. You should put yourself in the shoes of someone who has never run your code before, and describe the steps so they can copy and paste commands as required, or run pre-defined scripts. 

### Workload mandatory requirements

Each workload should meet the following minimal requirements to ensure that they can be run consistently and reproducibly. These include documentation, organisation, and Dockerfile/script formatting such that the CI pipeline in this repo can build and upload containers to the registry automatically for each task.

  - **Input data should be referenced** with links and download instructions, and not added to the repository unless they are very small (<1MB).
  - A **`README.md`** file containing at least:
	- A general description of the workload and purpose.
  	- Details of the software structure in your task, such that users understand how to tweak parameters and execute runs using alternative datasets. Versions and dependencies should be stated. Note that how the task is run should be abstracted as is described below when formating the Dockerfile.
	- A description of the expected outputs:
	  - Approximate runtime or performance results.
	  - Do not include output data products, but do include screenshots or values that show the expected result for a successful run.
	- Hardware dependencies. This involves specifying whether the code supports accelerators (such as GPUs), and whether these accelerators are required or optional. State any other hardware dependencies (e.g. if the code is only compatible with a specific network fabric, detail this).
  - Required files: **./Dockerfile**, **./run.sh**, **/scripts**, and a script set as the entrypoint in the Dockerfile (e.g. **/scripts/pipeline.sh**) to run the task.
    - The dockerfile should set `workdir=./scripts`, and this dir should contain everything that runs your task. This is so the top level task directory only has the run.sh script in it to avoid clutter. The entrypoint should be set in the Dockerfile, `entrypoint=["./pipeline.sh"]` so that we can use `docker run` to execute the tasks consistently. `/scripts/pipeline.sh` is just an example, and could be a Python script or any other script that runs the task. For example, you could have calls to `sourcefinding-large.py` and `sourcefinding-small.sh` inside of `pipeline.sh` to give examples running on large and small data sets. Other directories could be mounted if you need to store intemediate data products or outputs in a cleaner way.
  	- A script called `run.sh` in the top level dir that does a `docker pull` to check it's up to date from the registry (no local building is required), and does a `docker run -it --rm ...` such that when the container is run, it will execute the `./pipeline.sh` script (because of the entrypoint in the dockerfile), and shut down after completing this script. See existing tasks where you can copy/past this `run.sh` script and change the container name.
  - You can always do a `docker exec` command to use the container interactively for testing/debugging, but note that you will need to shut it down by hand afterwards, otherwise `docker run` commands will complain it is already running.
  - For the CI pipeline in this repo to automatically upload your container to the registry, your container should be named/tagged the same as the task name and **it must be all lower-case**.
  - Please reach out to a maintainer if you need assistance with any of these steps. Adding a new task means you are happy to collaborate on maintaining it and discussing updates via merge requests. 

**Please see the mosaicking-swarp, image-coadding-swarp, source-finding-pybdsf and image-cutouts-astropy tasks for examples.**

### Workload optional data

  In addition, we recommend adding the following optional information to each workload. These are especially relevant for tasks:

  - *In the README.md*: How to use the task with alternative data, e.g. editing relevant parameter files and highlighting any potential problems with unique data sets.
  - *Linked in the README.md*: A recorded demo of how to run the task.
  - *In a `contrib/` directory*: Include any relevant runtime parameters such as CPU affinity settings, recommended memory/core ratio, and performance scalability properties of the workload (strong scaling and/or weak scaling). You're welcome to include any data that helps understand the workload behaviour and aid in running it efficiently, such as batch submission scripts and/or graphs.
  - *In a `contrib/` directory*: Power and energy efficiency data (aggregated or time series).
  - *In a `contrib/` directory*: Profiling data, and accompanying performance analysis results or conclusions.
  - Automatic running of the workload in a remote site via CI pipelines.
  - A build script in `scripts/build.sh`, meant for end users who want to build the container images themselves. Note that the CI system will not use this script, so the container needs to build as described above.

## Directory structure

In order to be able to structure and scale all of this information, this repository is organised in the following hierarchical structure. The mosaicking-swarp Task is expanded as an example, with items in bold being mandatory:

<pre>
├── README.md
├── Tasks  # Tasks are independent steps towards a scientific result.
│   ├── Image-coadding-swarp
│   ├── Source-finding-pybdsf
│   ├── Image-cutouts-astropy
│   └── Mosaicking-swarp
│       ├── <b>Dockerfile</b>
│       ├── <b>run.sh</b>
│       ├── <b>README.md</b>
│       ├── <b>contrib</b>
│       └── <b>scripts</b>
│          ├── <b>pipeline.sh</b>
│          ├── make-lofar-mosaic.sh
│          ├── make-sdss-mosaic.sh
│          ├── lofar
│          │   ├── get-lofar-data-parallel.sh
│          │   └── lofar-files.csv
│          └── sdss
│              ├── get-sdss-data-parallel.sh
│              └── J011700.00+280000.0.small.sh
│   
├── Workflows # Workflows are a collection of independent tasks, with scripts that wrap them together so a user can obtain a complex science result.
│   ├── HI-FRIENDS
│   ├── eMerlin
│
└── System-tests
    ├── Distributed-analysis-dask
</pre>

We expect to organise tasks further into categories. It is unclear how broad these categories will be. We plan to decide this once we gather a significant amount of tasks, and it becomes more apparent how to scale these into categories.

Workflows consists of one or more tasks, and each task runs one application with certain inputs. Each task can be run multiple times, resulting in individual runs where certain parameters (input stays the same) may change, such as number of nodes/processes, the SRC node, or on the computation device. Workflows may be maintained as separate git repositories. Ideally, workflows reference any tasks used from this repository, but could include more unique aspects inside the workflow repository. For example, if a workflow does mosaicking, source-finding and source classification, each of those steps would be separate tasks, and they would be brought together using a workflow description language that references the tasks and provides all the information required to stitch them together into a complete workflow run. A workflow may include tasks that are not yet integrated into this repository, and so this should be stated clearly in the workflow with a note on if it makes sense to include this as a task in the future. 

Note: The exact structure for workflows is yet to be determined once we have a fully working example.

## Adding a new Task or Workflow

Adding a new Task or Workflow should replicate the example structure detailed above. We want all Tasks to be directly hosted in this repo, but exceptions can be made where it is more appropriate to add them as sub-modules that mirror another repo. Large workflows may already be hosted in other Git repositories, and so adding them as sub-modules may be preferred. Note however that in the future we plan to use CI/CD to build containers automatically and batch test tasks, which requires the task to be directly hosted in this repo. If you are on the SKAO Gitlab account you will be a "developer" who can create feature branches and do merge requests. A "maintainer" will then approve the merge request. 

To add a new Task, you should do the following from the command line:

Create a new feature branch and then submit a merge request following these instructions: https://docs.gitlab.com/ee/gitlab-basics/feature_branch_workflow.html

At stage 4 of those instructions, if you need to add it as a sub-module do the following (however we are discouraging the use of sub-modules, since it will not work with our CI pipeline and we would like all code hosted directly in this repo):

Move to the task directory:

`cd src-workloads/tasks`

Add the Git repo for your task as a sub module, where the path is the http url, and give it an appropriate folder name:

`git submodule add <path-to-your-task-repo> <folder-name-for-task>`

Then continue with steps 5/6/7/8/9 in the above instructions to push the changes to the branch you created and then submit a merge request to the main branch. A maintainer will then approve your merge request and the branch will be merged into the main. 


## Limitations and future challenges

We're aware of the following limitations and challenges that will have to be overcome as we attempt to run these workloads accross the SRCNet:

- Most HPC centers do not support Docker runtime at all (Docker **images** are usually fully supported). Often, only other runtime engines (such as Singularity/Apptainer or Sarus) are supported for security and operational reasons.  Therefore, some generic way of launching workloads will have to be found that goes beyond the current `run.sh` approach. We can't just have a `docker run` and expect things to work on remote sites.
	- One simple option would be using a shim wrapper.
	- Another solution would be leveraging future Compute Services APIs to launch a workload on remote SRCNet sites.
- Containers are built with portability in mind, which can have a severe negative impact on performance. This is not a specific issue to this project, but more globally for the SRCNet and the scientific/HPC community. For instance, solutions will have to be found to avoid having containers use the most common denominator that will run anywhere, which would by definition not be able to exploit the full performance offered by sites. Conversely, if a container is built with certain performance optimizations, this might exclude certain sites from being able to run this optimized workload. Therefore, a mechanism will be needed to select compatible sites for a given workload. Examples of optimizations that attempt to achieve high performance include (not an exhaustive list):
	- Using an appropriate MPI runtime, especially the networking components, can have a huge impact on network latency and throughput for MPI applications.
	- CPU codes that are compiled to use certain accelerated instructions (such as the AVX family, but there are others), means that a certain CPU "flavour" is required.
	- GPU codes built with a specific compiler version might have a minimal dependency on system software. (e.g. an nvidia compiler usually requires a minimal CUDA version).
